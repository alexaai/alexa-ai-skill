'use strict';

const Alexa = require('alexa-sdk');
const constants = require('./constants/constants');
const homeHandlers = require('./handlers/homeHandlers');
const aiHandlers = require('./handlers/aiHandlers');
const mythHandlers = require('./handlers/mythHandlers');
const awsHandlers = require('./handlers/awsHandlers');
const slalomHandlers = require('./handlers/slalomHandlers');
const questionnaireHandlers = require('./handlers/questionnaireHandlers');
// const apiCalls = require('./utilities/apiCalls');

exports.handler = function(event, context, callback) {
  let alexa = Alexa.handler(event, context);

  alexa.appId = constants.appId;
  alexa.registerHandlers(homeHandlers, aiHandlers, awsHandlers, slalomHandlers, questionnaireHandlers, mythHandlers);
  alexa.execute();
};
