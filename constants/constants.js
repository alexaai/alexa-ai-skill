module.exports = {
  appId: '', // need to add appID
  dynamoDBTableName: 'SlalomAISkills',
  states: {
    HOME: '',
    AI: 'ai',
    MYTH: 'myth',
    AWS: 'aws',
    QESTIONNAIRE: 'questionnaire',
    SLALOM: 'slalom',
    BUSINESS: 'business' // stretch goals
  }
};
