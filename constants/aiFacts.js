// TODO:
// need content for the facts (Sources, words, etc.)
module.exports = {
  'machine learning': `Machine learning takes the raw data to learn features in data that map to
    a model of the real world. Common use cases include: classification and
    detection, segmentation, text embedding, sentiment analysis, machine
    translation and more.`,
  'natural language processing': `Natural Language Processing allows us to process large volumes of text
    data and documents to categorize, analyze, and synthesize new insights.`
};
