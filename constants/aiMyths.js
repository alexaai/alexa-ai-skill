// TODO:
// Need content for this
module.exports = {
  'AI replacement':
    "Specialized AI should instead be celebrated for its ability to empower and drive higher efficiency in existing processes, and unlock people's full potential for creativity by minimizing menial or repetitive tasks. It's the difference from making a strategic short-term decision based on an automated report vs. a full team that's \"still crunching the numbers.\"",
  'AI is too hard':
    'Off the shelf has a whole new meaning for AI, with many platforms offering plug-and-play capabilities that require little upkeep, allowing teams to confidently adopt the latest solutions. Managed services and platforms allow clients to "dip their toes in," and still scale those frameworks to realize global-scale production use cases.',
  'no help needed':
    'With the pace of innovation in the overall ecosystem and market, no one is exempt from continuing advancement, and all clients should have the confidence and curiosity to adopt the latest technologies to maximize outcomes, ahead of the general curve.',
  'destructive AI':
    'Despite what you may hear in the news, the reality is that "generalized AI" is still a long way in the future. "Specialized-AI" in comparison is growing rapidly, as it is focused around enabling specific capabilities, operating within human-defined guardrails.',
  'too late':
    'Most clients have already started their AI journey and may have building blocks of specialized AI already in place without knowing it. Whether its building on existing capabilities in big data, cloud, or analytics, it’s easy to start small with AI.',
  'not mature':
    'The ecosystem for AI is in its "hockey stick" phase; never has there been more feature advancements paired with adoption of capabilities in production use cases. It\'s no longer a "fun idea" but a strategic business advantage that can be deployed with immediate value – with real partnerships, platforms, and people.',
  fad:
    'AI is not a platform, technology, or any "one thing", but rather a fundamentally different way to conduct business; one that drives value in virtually every capacity and impacts both people and processes in every industry.',
  'all or nothing':
    'Like other products and platforms, AI has a complexity scale, allowing clients to tailor their investments over time and integrate with existing solutions to maximize re-use and operational scale.'
};
