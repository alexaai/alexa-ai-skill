module.exports = {
    'sagemaker':
        `<prosody pitch="medium">
            Sage Maker allows you to quickly, and easily build, train, and deploy machine learning models at scale.
        </prosody>`,
    'deeplens':
        `<prosody pitch="medium">
            Deep lens is the world's first deep learning enabled video camera.
        </prosody>`,
    'lex':
        `<prosody pitch="medium">
            Lex is a set of conversational interfaces you can use in your applications.
        </prosody>`,
    'rekognition':
        `<prosody pitch="medium">
            Rekognition is a service that makes it easy to add image and video analysis to your applications.
        </prosody>`,
    'comprehend':
        `<prosody pitch="medium">
            Comprehend is a service that enables you to find insights, and relationships in text
        </prosody>`,
    'transcribe':
        `<prosody pitch="medium">
            Transcribe is a service that easily implements speech to text in your applications
        </prosody>`,
    'translate':
        `<prosody pitch="medium">
            Translate is a service that delivers fast, high quality, and affordable language translation
        </prosody>`,
    'polly':
        `<prosody pitch="medium">
            Polly is a service that reads text into lifelike speech.
        </prosody>`
};