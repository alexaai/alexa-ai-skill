module.exports = {
    'sagemaker':
        `<prosody pitch="medium">
            <s>
                Amazon SageMaker enables data scientists and developers to quickly and easily build, 
                train, and deploy machine learning models. 
            </s> 
            <s>   
                It offers high performance machine learning algorithms, broad framework support, 
                and one-click training, tuning, and inference. 
            </s>
            <s>
                SageMaker has a modular architecture so that you can
                use any or all of its capabilities in your existing machine learning workflows.
            </s>
        </prosody>`,
    'deeplens':
        `<prosody pitch="medium">
            <s>
                Amazon's Deep Lens is the world's first deep learning enabled video camera for developers. 
            </s>
            <s>
                It is integrated with Amazon SageMaker and many of Amazon's other web services; 
                allowing you to get started with deep learning quickly and easily.
            </s>
        </prosody>`,
    'lex':
        `<prosody pitch="medium">
            <s>
                Amazon Lex is a service used to create conversational voice and text interfaces for applications.
            </s>
            <s>
               Lex provides advanced deep learning functionalities of automatic speech recognition for 
               converting speech to text, and natural language understanding to recognize the intent of the text.
            </s> 
            <s>
                These enable you to build applications with highly engaging user experiences and life like conversations. 
 
            </s> 
            <s>
                With Lex, the same deep learning technologies that power Amazon's Alexa are available 
                to your developers, allowing them to quickly and easily build sophisticated, natural language 
                conversational bots.
            </s> 
            <s> 
                Commonly known as “chatbots”.
            </s>
        </prosody>`,
    'rekognition':
        `<prosody pitch="medium">
            <s>
               Amazon Rekognition makes it easy to add video and image analysis to your applications.
            </s> 
            <s>
               All you have to do is provide an image or video to the Rekognition API, 
               and the service can identify the objects, people, text, scenes, and activities.
            </s>
            <s>
               You can also detect any in appropriate content.
            </s>
            <s>
                Rekognition also provides highly accurate facial analysis and facial recognition.
            </s>
            <s>
                You'll be able to detect, analyze, and compare faces for 
                user verification, cataloging, people counting, or public safety.
            </s>
        </prosody>`,
    'comprehend':
        `<prosody pitch="medium">
            <s>
                Amazon Comprehend is a natural language processing service that uses 
                machine learning to find insights and relationships in text. 
            </s>    
            <s>
                Comprehend can identify the language of the text; 
                </s><s>extract key phrases, places, people, brands, or events; 
                </s><s>understand how positive or negative the text is; 
                </s><s>and automatically organize a collection of text files by topic.
            </s>
        </prosody>`,
    'transcribe':
        `<prosody pitch="medium">
            <s>
                Amazon Transcribe is an automatic speech recognition service that makes it easy 
                for developers to add speech to text capabilities to their applications. 
            </s>
            <s>
                Using the Transcribe API, you'll be able to analyze audio files stored in 
                Amazon S3 services, and have a text file of the transcribed speech returned.
            </s>
        </prosody>`,
    'translate':
        `<prosody pitch="medium">
            <s>
                Amazon Translate is a neural machine translation service that delivers fast, high quality, 
                affordable language translation.
            </s>
            <s>
                It's a form of language translation automation that uses machine learning and 
                deep learning models to deliver more accurate and natural sounding translations compared
                to traditional statistical and rule based translation algo-rithms.
            </s>
             <s>
                This allows you to easily translate large volumes of text efficiently; 
                enabling you to localize websites and applications for international users. 
            </s>
        </prosody>`,
    'polly':
        `<prosody pitch="medium">
            <s>
                Amazon Polly is a service that turns text into life like speech, allowing you 
                to create applications that talk, and build entirely new categories of speech enabled products. 
            </s>    
            <s>
                Polly is a Text-to-Speech service that uses advanced deep learning technologies 
                to synthesize speech that sounds natural. 
            </s>  
            <s>  
                With dozens of life like voises in a variety of languages, you can select the 
                ideal voice and build speech enabled applications that work around the world.
            </s>
        </prosody>`
};