// TODO:
// remove inline prompts

const Alexa = require('alexa-sdk');
const constants = require('../constants/constants');
const aiFacts = require('../constants/aiFacts');
const speechHelper = require('../utilities/speechHelper');
const alexaHelper = require('../utilities/alexaHelper');
const genericHandlers = require('../utilities/genericHandlers');
const prompts = require('./prompts/aiPrompts');

const LASTREQUEST = 'lastRequestedFact';

module.exports = Alexa.CreateStateHandler(
  constants.states.AI,
  Object.assign({}, genericHandlers, {
    GiveAIFacts: function() {
      const fact = alexaHelper.ResolveSlotValue('aifacts', this.event).toLowerCase();
      const friendlyFacts = speechHelper.SpeakPrettyFromArray(Object.keys(aiFacts));
      console.log(fact);
      if (fact in aiFacts) {
        this.attributes[LASTREQUEST] = fact; // for repeat request
        this.emit(':ask', prompts.prompt.GiveAIFacts(aiFacts[fact]), prompts.reprompt.GiveAIFacts);
      } else {
        this.emit(':ask', prompts.prompt.GiveAIFactsFail(friendlyFacts), prompts.prompt.GiveAIFactsFail(friendlyFacts));
      }
    },

    EnterState: function() {
      const facts = speechHelper.SpeakPrettyFromArray(Object.keys(aiFacts));
      this.emit(':ask', prompts.prompt.EnterState(facts), prompts.reprompt.EnterState);
    },

    'AMAZON.HelpIntent': function() {
      this.emit(':ask', prompts.prompt.HelpIntent);
    },

    Unhandled: function() {
      this.response.speak(prompts.prompt.Unhandled).listen(prompts.prompt.Unhandled);
      this.emit(':responseReady');
    }
  })
);
