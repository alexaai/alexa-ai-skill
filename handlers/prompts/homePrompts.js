module.exports = {
    'prompt': {
        'LaunchRequest': `
            <prosody pitch="medium">
                Welcome to Slalom A.I.'s skill for AWS! Would you like to hear about A.I. facts, A I myths, or AWS's A.I offerings?    
            </prosody>`,
        'EnterState': `
            <prosody pitch="medium">
                Which topic would you like to hear about? 
                    <s>A.I. Facts</s>
                    <s>A I Myths</s> or 
                    <s>AWS's A.I. offerings.</s>  
            </prosody>`,
        'HelpIntent': `
            <prosody pitch="medium">
                OK! You can ask me to tell you about 
                    <s>A.I. Facts</s>
                    <s>A I Myths</s> or 
                    <s>AWS's A.I. offerings.</s>  
                For example, you can say
                    <s>"Alexa, tell me about A.I. facts", to hear about A.I. Facts</s> or
                    <s>"Alexa, tell me more about AWS's A.I. offerings", to hear what Amazon offers in the field of A.I.</s>
            </prosody>`,
        'CatchState': `
            <prosody pitch="medium">
                Would you like to exit? You can say, "Alexa, stop". 
            </prosody>`
        },
    'reprompt': {
        'LaunchRequest': `
            <prosody pitch="medium">
                I can tell you about
                    <s>A.I. Facts</s>
                    <s>A I Myths</s> or 
                    <s>AWS's A.I. offerings.</s>
            </prosody>`,
        'EnterState': `  
            <prosody pitch="medium">
                I can tell you more about,
                    <s>A.I. Facts</s>
                    <s>A I Myths</s> or 
                    <s>AWS's A.I. offerings.</s>
            </prosody>`,
        'HelpIntent': `
            <prosody pitch="medium">
                <s>For A.I. Facts, say</s> 
                    <s>"Alexa, tell me about A.I. facts"</s>
                <s>For A I Myths, say</s> 
                    <s>"Alexa, tell me about A I Myths"</s>
                <s>For AWS's A.I. offerings, say</s>  
                    <s>"Alexa, tell me about AWS's A.I. offerings"</s>
            </prosody>`,
        'CatchState': `
            <prosody pitch="medium">
                <s>If you'd like to exit say, "Alexa, stop"</s>
                <s>Other wise you can reply by saying, <emphasis level='reduced'>"no"</emphasis>, and I'll repeat the available options</s>
            </prosody>`
    }
};