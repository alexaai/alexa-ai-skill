module.exports = {
  prompt: {
    EnterState: (facts) => `AI or artificial intelligence is a complex topic, I can give you more details
    about the following topics: ${facts}`,
    HelpIntent: `Hi, which AI fact would you like to hear more about?`,
    GiveAIFacts: (fact) => `${fact} Is there any other topic area that I could give you information about?`,
    GiveAIFactsFail: (facts) => `Sorry, I don't know about that topic.  We could talk about: ${facts}`
  },
  reprompt: {
    GiveAIFacts: `What topic can I give you details about?`,
    EnterState: `What AI topic can I give you details about?`,
    HelpIntent: `You're able to ask for specific offerings. Which aws offering would you like to hear about?`,
    Unhandled: `That's also not an option. If you need to hear the AI topics, please ask. Or ask about something else.`
  }
};
