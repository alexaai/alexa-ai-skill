module.exports = {
  prompt: {
    GiveAIMyths: (myth) => `${myth} Are there any other myths you'd like to hear about?`,
    GiveAIMythsFail: (mythLabels) => `Sorry, I've never heard of that myth.  You could ask me about: ${mythLabels}`,
    EnterState: (mythLabels) =>
      `There are many AI myths, some myths we can discuss further are ${mythLabels}. Which of these myths would you like to hear more about?`
  },
  reprompt: {
    GiveAIMyths: `Was there something else I could help you with?`,
    GiveAIMythsFail: `What myths or misconceptions about AI would you like to hear more about?`,
    EnterState: `Did you have any more questions about these myths?`
  }
};
