module.exports = {
    'prompt': {
        'EnterStateOffering': (offerings) => `AWS Provides various Machine Learning services and capabilities, 
                                     such as ${offerings}`,
        'EnterState':
            `<prosody pitch="medium">
                Which offering would you like to learn about?
            </prosody>`,
        'GiveAWSDetailsSuccess':
            `<prosody pitch="medium">
                Would you like to know more?
            </prosody>`,
        'GiveAWSDetailsFail':
            `<prosody pitch="medium">
                Sorry, that wasn't a valid offering. Which offering would like to hear about?
            </prosody>`,
        'ExpandAWSDetails':
            `<prosody pitch="medium">
                Would you like me to repeat that?
            </prosody>`,
        'HelpIntent':
            `<prosody pitch="medium">
                OK, you can ask me for an offering by saying, 
                <s>"Alexa, tell me about" and then say the name of the offering you would like to learn about.</s>
            </prosody>`,
        'CatchState': `
            <prosody pitch="medium">
                Would you like to exit? You can say, "Alexa, Stop".
            </prosody>`
    },
    'reprompt': {
        'EnterState':
            `<prosody pitch="medium">
                Is there a specific offering that you'd like to learn about?
            </prosody>`,
        'GiveAWSDetailsSuccess':
            `<prosody pitch="medium">
                What else would you like to learn about?
            </prosody>`,
        'GiveAWSDetailsFail':
            `<prosody pitch="medium">
                <s>You can ask about: </s>
            </prosody>`,
        'ExpandAWSDetails':
            `<prosody pitch="medium">
                <s>Are there any other offerings that you're interested in?</s>
            </prosody>`,
        'HelpIntent':
            `<prosody pitch="medium">
                <s>Which A.W.S. offering would you like to hear about?</s>
                <s>Here's a set of examples for what you can ask me:</s> 
            </prosody>`,
        'CatchState': `
             <prosody pitch="medium">
                <s>Would you like to exit? Just say yes, or tell me to Stop.</s>
                <s>If you'd like to know how to ask me something related to A.W.S., you can say</s>
                <s>"Alexa, I need help."</s> 
                <s>
                    If you'd like to talk about something else, you can ask me to tell you about
                    A.I. facts, or A I myths.
                </s>
            </prosody>`
    }
};