module.exports = {
  prompt: {
    StopIntent: 'Goodbye',
    CancelIntent: 'Ok',
    Unhandled: 'Sorry, that option is not available. Is there something else I can help you with?'
  },
  reprompt: {}
};
