const Alexa = require('alexa-sdk');
const constants = require('../constants/constants');
const aiMyths = require('../constants/aiMyths');
const genericHandlers = require('../utilities/genericHandlers');
const prompts = require('./prompts/mythPrompts');
const alexaHelper = require('../utilities/alexaHelper');
const speechHelper = require('../utilities/speechHelper');

const LASTREQUEST = 'lastRequestedMyth';

module.exports = Alexa.CreateStateHandler(
  constants.states.MYTH,
  Object.assign({}, genericHandlers, {
    GiveMythDetails: function() {
      const prettyMyths = speechHelper.SpeakPrettyFromArray(Object.keys(aiMyths));
      const myth = alexaHelper.ResolveSlotValue('aimyths', this.event);
      if (myth in aiMyths) {
        this.attributes[LASTREQUEST] = myth; // for repeat request
        this.emit(':ask', prompts.prompt.GiveAIMyths(aiMyths[myth]), prompts.reprompt.GiveAIMyths);
      } else {
        this.emit(':ask', prompts.prompt.GiveAIMythsFail(prettyMyths), prompts.reprompt.GiveAIMythsFail);
      }
    },

    EnterState: function() {
      const prettyMyths = speechHelper.SpeakPrettyFromArray(Object.keys(aiMyths));
      this.emit(':ask', prompts.prompt.EnterState(prettyMyths), prompts.reprompt.EnterState);
    }
  })
);
