'use strict';

const Alexa = require('alexa-sdk');
const constants = require('../constants/constants');
const genericHandlers = require('../utilities/genericHandlers');
const prompts = require('./prompts/homePrompts');

const INTENT = 'home.intent';
const intents = {
    'LaunchRequest':'LaunchRequest',
    'EnterState':'EnterState',
    'CatchState': 'CatchState',
    'HelpIntent':'HelpIntent',
    'Unhandled':'Unhandled'
};

module.exports = Alexa.CreateStateHandler(
  constants.states.HOME,
    Object.assign({}, genericHandlers, {
    'LaunchRequest': function() {
      this.attributes[INTENT] = intents.LaunchRequest;
      this.emit(':ask', prompts.prompt.LaunchRequest, prompts.reprompt.LaunchRequest);
    },

    'EnterState': function() {
        this.attributes[INTENT] = intents.EnterState;
        this.emit(':ask', prompts.prompt.EnterState, prompts.reprompt.EnterState)
    },

    'CatchState': function() {
      this.attributes[INTENT] = intents.CatchState;
      this.emit(':ask', prompts.prompt.CatchState, prompts.reprompt.CatchState)
    },

    'AMAZON.HelpIntent': function() {
        this.attributes[INTENT] = intents.HelpIntent;
        this.emit(':ask', prompts.prompt.HelpIntent, prompts.reprompt.HelpIntent);
    },

    'AMAZON.YesIntent': function() {
        switch(this.attributes[INTENT]) {
            case intents.LaunchRequest:
                this.emitWithState('EnterState');
                break;
            case intents.EnterState:
                this.emitWithState('EnterState');
                break;
            case intents.CatchState:
                this.emit('Amazon.StopIntent');
                break;
            case intents.HelpIntent:
                this.emitWithState('AMAZON.HelpIntent');
                break;
            default:
                this.emit('Unhandled');
        }
     },

    'AMAZON.NoIntent': function() {
        switch(this.attributes[INTENT]) {
            case intents.LaunchRequest:
                this.emitWithState('CatchState');
                break;
            case intents.EnterState:
                this.emitWithState('CatchState');
                break;
            case intents.HelpIntent:
                this.emitWithState('CatchState');
                break;
            case intents.CatchState:
                this.emitWithState('EnterState');
                break;
            default:
                this.emit('Unhandled');
        }
     }
  })
);
