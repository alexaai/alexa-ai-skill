const constants = require('../constants/constants');
const alexaHelper = require('./alexaHelper');
const prompts = require('../handlers/prompts/genericPrompts');

module.exports = {
  // Switches to the state ther user has requested.
  SwitchState: function() {
    const state = alexaHelper.ResolveSlotValue('states', this.event).toUpperCase();
    this.handler.state = constants.states[state];
    console.log('generic handler switching state to:')
    console.log(state);
    if(state)
      this.emitWithState('EnterState');
    else
      this.emit(':ask', prompts.prompt.Unhandled)
  },
  'AMAZON.StopIntent': function() {
    this.emit(':tell', prompts.prompt.StopIntent);
  },
  'AMAZON.CancelIntent': function() {
    this.emit(':tell', prompts.prompt.CancelIntent);
  },
  'AMAZON.NoIntent': function() {
    this.handler.state = constants.states.HOME;
    this.emitWithState('EnterState');
  },
  SessionEndedRequest: function() {
    this.emit(':saveState', true);
  },
  Unhandled: function() {
    this.emit(':ask', prompts.prompt.Unhandled, prompts.reprompt.Unhandled);
  }
};
